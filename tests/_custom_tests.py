# We must be able to import sqarf:
import sqarf


# We must be able to define a new test type:
class MyCustomTest(sqarf.QATest):
    def test(self, context):
        return True, "this is just an example..."


# We must be able to access the "SESSION" name in global scope:
current_session = SESSION


def get_root_tests():
    return (MyCustomTest,)
