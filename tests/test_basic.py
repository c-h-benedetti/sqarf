import sqarf


def test_readme_example():
    class IsEnough(sqarf.QATest):
        def test(self, context):
            value = context["value"]
            minimum = context["minimum"]
            if value > 2 * minimum:
                context["pass_iseven_test"] = True

            if value > minimum:
                return True, "Got more that {}.".format(minimum)
            else:
                return False, "{} is not enough !".format(value)

    class IsEven(sqarf.QATest):
        def test(self, context):
            if context.get("pass_iseven_test"):
                return True, "Test disabled by a previous test."
            if context["value"] % 2:
                return False, "not even a little bit even"
            else:
                return True, "even in even"

    class TestGroup(sqarf.QATest):
        def get_sub_test_types(self):
            return (IsEnough, IsEven)

        def test(self, context):
            context["minimum"] = 10
            return super(TestGroup, self).test(context)

    session = sqarf.Session()
    session.register_test_types(
        [
            TestGroup,
        ]
    )

    session.context_set(value=16)
    result = session.run()
    assert result.passed()

    session.context_set(value=5)
    result = session.run()
    assert result.failed()

    # This run will skip the IsEven test:
    session.context_set(value=32)
    result = session.run()
    assert result.passed()
