import pytest

from sqarf.context import Context


def test_base():

    base = Context()
    base[1] = 1
    base[2] = 2
    base[3] = 3
    assert base[1] == 1
    assert base[2] == 2
    assert base[3] == 3

    with pytest.raises(KeyError):
        del base[4]

    with pytest.raises(KeyError):
        base[5]


def test_get():

    base = Context()
    base[1] = 1
    base[2] = 2

    ctx = Context(base)
    ctx[2] = "over"
    ctx[3] = "add"

    assert ctx.get(1) == 1
    assert ctx.get(2) == "over"
    assert ctx.get(3) == "add"
    assert ctx.get(4) is None

    with pytest.raises(KeyError):
        ctx[4]


def test_context_del():

    base = Context()
    base[1] = 1
    base[2] = 2

    ctx = Context(base)
    del ctx[2]
    ctx[3] = "add"

    assert ctx.to_dict() == {1: 1, 3: "add"}

    with pytest.raises(KeyError):
        del ctx["no_such_key"]

    with pytest.raises(KeyError):
        del ctx[2]

    ctx[2] = "over"
    assert ctx.to_dict() == {1: 1, 2: "over", 3: "add"}
    assert ctx[2] == "over"
    del ctx[1]  # should not raise
    with pytest.raises(KeyError):
        assert ctx.to_dict()[1]


def test_context_base_del():

    base = Context()
    base[1] = 1
    base[2] = 2
    base[3] = 3

    ctx = Context(base)
    del ctx[2]

    assert ctx.to_dict() == {1: 1, 3: 3}
    assert base.to_dict() == {1: 1, 2: 2, 3: 3}


def test_context_base_add():

    base = Context()
    base[1] = 1
    base[2] = 2

    ctx = Context(base)
    ctx[3] = 3

    assert ctx.to_dict() == {1: 1, 2: 2, 3: 3}
    assert base.to_dict() == {1: 1, 2: 2}


def test_context_base_over():

    base = Context()
    base[1] = 1
    base[2] = 2

    ctx = Context(base)
    ctx[2] = "OVER"

    assert ctx.to_dict() == {1: 1, 2: "OVER"}
    assert base.to_dict() == {1: 1, 2: 2}


def test_to_dict():
    base = Context()
    base[1] = 1
    base[2] = 2
    base[3] = 3

    ctx = Context(base)
    del ctx[2]
    ctx[3] = "OVER"
    ctx[4] = 4

    assert ctx.to_dict() == {1: 1, 3: "OVER", 4: 4}


def test_to_edits():

    base = Context()
    base[1] = 1
    base[2] = 2
    base[3] = 3

    ctx = Context(base)
    del ctx[2]
    ctx[3] = "OVER"
    ctx[4] = 4

    assert ctx.edits() == ({4: 4}, {3: "OVER"}, [2])
