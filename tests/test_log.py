from sqarf.log import Log


def test_log_title():
    log = Log("My Title")
    assert log.pformat(indent_string="  ") == "### My Title\n"


def test_log_info():
    log = Log()
    message = "This is an informative message."
    log.info(message)

    assert log.pformat(indent_string="  ") == "### Log\n  " + message + "\n"


def test_log_warning():
    log = Log()
    message = "Watch out !"
    log.warning(message)

    assert (
        log.pformat(indent_string="  ")
        == "### Log (1 Warnings)\n  [!] " + message + "\n"
    )


def test_log_error():
    log = Log()
    message = "Watch out !"
    log.error(message)

    assert (
        log.pformat(indent_string="  ")
        == "### Log (1 Errors)\n  [!!!] " + message + "\n"
    )


def test_log_section():

    section_title = "Section Test"
    message = "message..."

    log = Log()
    with log.section(section_title) as section:
        section.info(message)
        section.warning(message)
        section.warning(message)
        with section.section(section_title) as section:
            section.error(message)

    expected = (
        "### Log (1 Errors, 2 Warnings)\n"
        "    ### " + section_title + " (1 Errors, 2 Warnings)\n"
        "        " + message + "\n"
        "        [!] " + message + "\n"
        "        [!] " + message + "\n"
        "        ### " + section_title + " (1 Errors)\n"
        "            [!!!] " + message + "\n"
    )
    assert log.pformat(indent_string="    ") == expected


def test_debug_output():

    log = Log()
    log.info("Info 1")
    log.debug("Debug")
    log.info("Info 2")

    assert log.pformat() == "### Log\n    Info 1\n    Info 2\n"
    assert (
        log.pformat(include_debug=True)
        == "### Log\n    Info 1\n    >>> Debug\n    Info 2\n"
    )


def test_usage():

    root = Log("My Log")

    log = root
    with log.section("Section 1") as log:
        log.info("We are doing something here:")
        log.info("It's about logging text")
        log.warning("In a stuctured way")
        with log.section("Available messages types:") as log:
            log.info("section (like the line above)")
            log.info("info")
            log.warning("warning")
            log.error("error")

    print("\nHere is the `to_line()` return value:")
    for indent, message_type, message in root.to_lines():
        print("  ", (indent, message_type, message))

    print("\nHere is the `pformat()` return value:")
    print(root.pformat())
