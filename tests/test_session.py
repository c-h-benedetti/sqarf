import os

import sqarf


def test_register_tests_from_file():
    session = sqarf.Session()
    assert not session._test_types

    filename = os.path.normpath(os.path.join(__file__, "..", "_custom_tests.py"))
    session.register_tests_from_file(filename)

    assert session._test_types
    for TestType in session._test_types:
        assert issubclass(TestType, sqarf.QATest)


TEST_CODE = """
# We must be able to import sqarf:
import sqarf

# We must be able to define a new test type:
class MyCustomTest(sqarf.QATest):
    def test(self, context):
        return True, "this is just an example of test definition string..."

# We must be able to access the "SESSION" name in global scope:
current_session = SESSION

def get_root_tests():
    return (MyCustomTest,)
"""


def test_register_tests_from_string():

    session = sqarf.Session()
    assert not session._test_types

    code = TEST_CODE
    filename = "dynamic_code"
    session.register_tests_from_string(code, filename)

    assert session._test_types
    for TestType in session._test_types:
        assert issubclass(TestType, sqarf.QATest)
